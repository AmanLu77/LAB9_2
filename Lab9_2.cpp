#include <iostream>
#include <inc/BMP.hpp>

/*
��������� ����������� ����, ������� ��� �������� �� ������.

��� ���������� ��� �������������:
1.	�������� ������ BMP. +
2.	����������� ������� ������ � ������ �����������. +
3.	��������� ���� �� �������������� ������� �� ������ ����������. +
4.	�������������� ���������� �� ������ ����������. +
5.	�������� 1 ������ ��� �����������. +

� ������� BMP http://4a4ik.blogspot.com/2014/08/bmp.html
*/

int main()
{
    try
    {
        BMP my_bmp;
        my_bmp.ReadFile("input.bmp");
        my_bmp.Negative(); // my filter
        my_bmp.SaveFile("output.bmp");

        BMP test_bmp(1920, 1080);
        test_bmp.Fill({ 255,0,0 });
        test_bmp.SaveFile("test.bmp");

        BMP empty_bmp;
        empty_bmp = test_bmp;
        empty_bmp.SaveFile("empty.bmp");
        
        return 0;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
}